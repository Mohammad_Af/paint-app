package animation;

import java.io.IOException;

public interface Animatable {
    void step() throws IOException;
}
