package animation;

import shape.*;

public class ZoomAnimation extends Animation {

    private double nesbat;
    private Shape shape;
    private final double recHtoW;
    private final double firstradius;
    private final double firstRecWidht;
    private final double fistLineVector;
    private boolean increasing = true;

    public ZoomAnimation(int stepDelay, double nesbat, Shape shape) {
        super(stepDelay);
        this.nesbat = nesbat;
        this.shape = shape;

        if (shape.getClass() == Rectangle.class) {
            recHtoW = ((Rectangle) shape).getHeight() / ((Rectangle) shape).getWidth();
            firstRecWidht = ((Rectangle) shape).getWidth();
        } else if (shape.getClass() == Image.class) {
            recHtoW = ((Image) shape).getHeight() / ((Image) shape).getWidth();
            firstRecWidht = ((Image) shape).getWidth();
        } else {
            recHtoW = firstRecWidht = 0;
        }

        if (shape.getClass() == Circle.class) {
            firstradius = ((Circle) shape).getRadius();
        } else firstradius = 0;

        if (shape.getClass() == Line.class) {
            fistLineVector = ((Line) shape).end.x - ((Line) shape).start.x;
        } else fistLineVector = 0;

    }


    @Override
    public void step() {


            if (!repeatable) {
                if (shape.getClass() == Circle.class && ((Circle) shape).getRadius() < nesbat * firstradius) {
                    ((Circle) shape).setRadius(((Circle) shape).getRadius() + 1);
                }
                if (shape.getClass() == Rectangle.class && ((Rectangle) shape).getWidth() < firstRecWidht * nesbat) {

                    ((Rectangle) shape).height += (recHtoW) * 1;
                    ((Rectangle) shape).width += 1;

                }
                if (shape.getClass() == Line.class && ((Line) shape).end.x < nesbat * fistLineVector + ((Line) shape).start.x) {

                    double a = (((Line) shape).end.y - ((Line) shape).start.y) / (((Line) shape).end.x - ((Line) shape).start.x);

                    ((Line) shape).end.y += 1 * a;
                    ((Line) shape).end.x += 1;


                }
                if (shape.getClass() == Image.class && ((Image) shape).getWidth() < firstRecWidht * nesbat) {

                    ((Image) shape).height += (recHtoW) * 1;
                    ((Image) shape).width += 1;
                }

            } else {

                if (shape.getClass() == Circle.class) {
                    if (((Circle)shape).getRadius() > firstradius * nesbat && increasing) {
                        increasing = false;
                    }
                    if (((Circle) shape).getRadius() < firstradius && !increasing) {
                        increasing = true;
                    }
                    if (increasing) {
                        ((Circle) shape).setRadius(((Circle) shape).getRadius() + 1);
                    } else {
                        ((Circle) shape).setRadius(((Circle) shape).getRadius() - 1);
                    }
                }
                if(shape.getClass()== Rectangle.class){
                    if(((Rectangle) shape).getWidth() > firstRecWidht * nesbat && increasing){
                        increasing=false;
                    }
                    if(((Rectangle) shape).getWidth() < firstRecWidht  && !increasing){
                        increasing=true;
                    }
                    if(increasing){
                        ((Rectangle) shape).height += (recHtoW) * 1;
                        ((Rectangle) shape).width += 1;
                    }else {
                        ((Rectangle) shape).height -= (recHtoW) * 1;
                        ((Rectangle) shape).width -= 1;
                    }
                }
                if(shape.getClass()== Image.class){
                    if(((Image) shape).getWidth() > firstRecWidht * nesbat && increasing){
                        increasing=false;
                    }
                    if(((Image) shape).getWidth() < firstRecWidht  && !increasing){
                        increasing=true;
                    }
                    if(increasing){
                        ((Image) shape).height += (recHtoW) * 1;
                        ((Image) shape).width += 1;
                    }else {
                        ((Image) shape).height -= (recHtoW) * 1;
                        ((Image) shape).width -= 1;
                    }
                }
                if (shape.getClass() == Line.class ) {
                    if(((Line) shape).end.x > nesbat * fistLineVector + ((Line) shape).start.x && increasing){
                        increasing=false;
                    }
                    if(((Line) shape).end.x <  fistLineVector + ((Line) shape).start.x && !increasing){
                        increasing=true;
                    }
                    if (increasing){
                        double a = (((Line) shape).end.y - ((Line) shape).start.y) / (((Line) shape).end.x - ((Line) shape).start.x);

                        ((Line) shape).end.y += 1 * a;
                        ((Line) shape).end.x += 1;
                    }else {
                        double a = (((Line) shape).end.y - ((Line) shape).start.y) / (((Line) shape).end.x - ((Line) shape).start.x);

                        ((Line) shape).end.y -= 1 * a;
                        ((Line) shape).end.x -= 1;
                    }


                }

            }

    }

    public void setNesbat(double nesbat) {
        this.nesbat = nesbat;
    }

}
