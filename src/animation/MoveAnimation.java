package animation;

import shape.Point;
import shape.Shape;

public class MoveAnimation extends Animation {

    private Point vector;
    private Shape shape;
    private Point shapeFirstLocation;
    private boolean goingBack=false;

    public MoveAnimation(int stepDelay, Point vector, Shape shape) {
        super(stepDelay);
        this.shape=shape;
        this.vector=vector;
        shapeFirstLocation=shape.location;

    }

    @Override
    public void step() {                             //nagative vector????????


            if (!repeatable) {
                if (shape.location.x < shapeFirstLocation.x + vector.x) {
                    shape.location = shape.location.add(new Point(1*(vector.y/vector.x), 1*(vector.x/vector.y)));
                }

            } else {
                if(shape.location.x >= shapeFirstLocation.x+vector.x && !goingBack){
                    goingBack=true;
                }
                if(shape.location.x < shapeFirstLocation.x && goingBack){
                    goingBack=false;
                }
                if(goingBack){
                    shape.location=shape.location.subtract(new Point(1*(vector.y/vector.x),1*(vector.x/vector.y)));
                }
                if(!goingBack){
                    shape.location=shape.location.add(new Point(1*(vector.y/vector.x),1*(vector.x/vector.y)));
                }
            }

        }

    public void setVector(Point vector) {
        this.vector = vector;
    }
}
