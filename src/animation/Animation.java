package animation;

import java.io.IOException;
import java.util.Date;

public abstract class Animation implements Animatable {
    private boolean animating=true;
    private Date nextAnimation=new Date();
    private int stepDelay;
    boolean repeatable=false;

    Animation(int stepDelay){
        this.stepDelay=stepDelay;
    }


    public void animate() throws IOException {
        if(animating) {
            if (nextAnimation.before(new Date())) {
                step();
                nextAnimation.setTime(new Date().getTime() + stepDelay);
            }
        }
    }

    public boolean getAnimating() {
        return animating;
    }

    public void setAnimating(boolean animating) {
        this.animating = animating;
    }

    public void setStepDelay(int stepDelay) {
        this.stepDelay = stepDelay;
    }

    public boolean isAnimating() {
        return animating;
    }

    public boolean isRepeatable() {
        return repeatable;
    }

    public void setRepeatable(boolean repeatable) {
        this.repeatable = repeatable;
    }
}
