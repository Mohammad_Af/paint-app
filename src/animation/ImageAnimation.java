package animation;

import UI.panel.PaintPanel;
import shape.Image;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.io.File;
import java.io.IOException;


public class ImageAnimation extends Animation {

    private final shape.Shape shape;
    private int currentIndex = 0;
    public static PaintPanel showPanel;

    public ImageAnimation(int stepDelay) {
        super(stepDelay);
        this.shape = showPanel.getClickedShape();

        initializ(shape);

    }

    public static void initializ(shape.Shape shape) {
        JFrame frame = new JFrame();
        JPanel content = (JPanel) frame.getContentPane();
        frame.setSize(400, 150);
        frame.setLocation(600, 500);
        content.setLayout(new BoxLayout(content, BoxLayout.PAGE_AXIS));
        JPanel panel2 = new JPanel();
        panel2.setLayout(new GridBagLayout());
        panel2.add(new JLabel("how many pictures do yo want to add ?"));
        JPanel panel = new JPanel();
        panel.setLayout(new GridBagLayout());
        JTextField textField = new JTextField();
        textField.setPreferredSize(new Dimension(80, 30));
        textField.setFont(new Font("SansSerif", Font.BOLD, 20));
        panel.add(textField);
        JPanel panel1 = new JPanel();
        panel1.setLayout(new GridBagLayout());
        JButton button = new JButton("OK");
        panel1.add(button);
        button.addActionListener(e -> {

            frame.dispose();
            for (int i = 0; i < Integer.parseInt(textField.getText()); i++) {
                JFileChooser chooser = new JFileChooser();
                FileNameExtensionFilter filter = new FileNameExtensionFilter("image", "jpg", "png", "gif");
                chooser.setFileFilter(filter);
                if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                    try {
                        ((Image) shape).getImageList().add(ImageIO.read(new File(chooser.getSelectedFile().getAbsolutePath())));
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            }

        });

        content.add(panel2);
        content.add(panel);
        content.add(panel1);


        frame.setVisible(true);
    }


    @Override
    public void step() {

        Image image = ((Image) shape);

        if (!repeatable && currentIndex < image.getImageList().size()) {
            image.setImage(image.getImageList().get(currentIndex));
            currentIndex++;
            showPanel.repaint();

        } else {
            image.setImage(image.getImageList().get(currentIndex));
            currentIndex++;
            if (currentIndex == image.getImageList().size()) {
                currentIndex = 0;
            }
            showPanel.repaint();
        }
    }

}
