package shape;

import java.awt.*;

public class Line extends Shape implements Drawable{
    public Point end;
    public Point start;

    public Line(Point start,Point end) {
        this(start,end,Color.green);
        colorIndex=3;
    }

    private Line(Point start, Point end, Color solidColor) {
        super(start, solidColor);
        this.start=start;
        this.end=end;
        colorIndex=3;
    }

    @Override
    public boolean isIn(Point point) {
        double x1=start.x;
        double y1=start.y;
        double x2=end.x;
        double y2=end.y;
        double a=(y2-y1)/(x2-x1);
        double b=y2-a*x2;
        return (point.x>x1 && point.x<x2)&&(point.y>a*point.x+b-10 && point.y<a*point.x+b+10);
    }

    @Override
    public void render(Graphics2D G) {
        G.setStroke(new BasicStroke(thickness));
        G.setColor(this.solidColor);
        G.drawLine((int)location.getX(),(int)location.getY(),(int)end.getX(),(int)end.getY());
        G.setColor(Color.yellow);

    }

}
