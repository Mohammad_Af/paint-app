package shape;

import java.awt.*;

public interface Drawable {
    void render(Graphics2D G);
}
