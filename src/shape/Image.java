package shape;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class Image extends Shape {

    public double width;
    public double height;
    private BufferedImage image=ImageIO.read(new File("images"+File.separator+"WR1.png"));
    private List<BufferedImage> imageList=new ArrayList<>();




    public Image(Point upperLeft, double width, double height, Color solidColor) throws IOException {
        super(upperLeft,solidColor);
        this.width=width;
        this.height=height;
        imageList.add(image);
    }

    @Override
    public boolean isIn(Point point) {
        return (point.x < location.x+height/2 && point.x>location.x-height/2)
                &&(point.y < location.y+width/2 && point.y>location.y-width/2);
    }

    @Override
    public void render(Graphics2D G) {


        if(image==null)
        image=imageList.get(0);

        G.setColor(solidColor);
        G.drawImage(image,(int)(location.x-width/2),(int)(location.y-height/2),(int)width,(int)height,null);
        G.setStroke(new BasicStroke(thickness));
        G.setColor(borderColor);
        G.drawRect((int)(location.x-width/2),(int)(location.y-height/2),(int)width,(int)height);


    }

    public double getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public BufferedImage getImage() {
        return image;
    }

    public void setImage(BufferedImage image) {
        this.image = image;
    }

    public List<BufferedImage> getImageList() {
        return imageList;
    }
}
