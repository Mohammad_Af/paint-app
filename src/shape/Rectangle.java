package shape;

import animation.Animatable;

import java.awt.*;

public class Rectangle extends Shape implements Animatable {

    public double width;
    public double height;

    public Rectangle(Point upperLeft , double width, double height){
        this(upperLeft,width,height,Color.blue);
        colorIndex=2;
    }

    private Rectangle(Point upperLeft, double width, double height, Color solidColor) {
        super(upperLeft,solidColor);
        this.width=width;
        this.height=height;
        colorIndex=2;
    }

    @Override
    public boolean isIn(Point point) {
        return (point.x < location.x+width/2 && point.x>location.x-width/2)
                &&(point.y < location.y+height/2 && point.y>location.y-height/2);

    }

    @Override
    public void render(Graphics2D G) {

        G.setColor(solidColor);
        G.fillRect((int)(location.x-width/2),(int)(location.y-height/2),(int)width,(int)height);
        G.setStroke(new BasicStroke(thickness));
        G.setColor(borderColor);
        G.drawRect((int)(location.x-width/2),(int)(location.y-height/2),(int)width,(int)height);

    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }
}
