package shape;

import animation.Animatable;
import animation.Animation;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;

public abstract class Shape implements Animatable, Drawable {
    public Point location;
    public Color solidColor;
    public Color borderColor=null;
    int colorIndex;
    int thickness=5;
    private ArrayList<Animation> animations=new ArrayList<>();


    Shape(Point location, Color solidColor){
        this.location=location;
        this.solidColor=solidColor;
    }

    public void addAnimation(Animation animation){
        animations.add(animation);
    }

    @Override
    public void step() throws IOException {
        for (Animation animation : animations) {
            animation.animate();
        }
    }

    public abstract boolean isIn(Point point);

    public Point getLocation() {
        return location;
    }

    public void setLocation(Point location) {
        this.location = location;
    }

    public void setColorIndex(int colorIndex) {
        this.colorIndex = colorIndex;
    }

    public int getColorIndex() {
        return colorIndex;
    }

    public ArrayList<Animation> getAnimations() {
        return animations;
    }
}
