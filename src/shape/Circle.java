package shape;

import java.awt.*;

public class Circle extends Shape {

    private double radius=100;

    public Circle(Point center, double radius) {
        this(center,radius,Color.red);
        colorIndex=0;

    }

    private Circle(Point location, double radius, Color solidColor) {
        super(location, solidColor);
        this.radius=radius;
        colorIndex=0;
    }

    @Override
    public boolean isIn(Point point) {
        double dist=location.subtract(point).getRad();
        return dist<radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public void render(Graphics2D G) {
        G.setColor(solidColor);
        G.fillOval((int)(location.x-radius),(int)(location.y-radius),(int)radius*2,(int)radius*2);
        G.setStroke(new BasicStroke(thickness));
        G.setColor(borderColor);
        G.drawOval((int)(location.x-radius),(int)(location.y-radius),(int)radius*2,(int)radius*2);
    }

}
