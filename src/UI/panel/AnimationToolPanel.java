package UI.panel;

import UI.IconButton;
import animation.ImageAnimation;
import animation.MoveAnimation;
import animation.ZoomAnimation;
import shape.Image;
import shape.Point;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AnimationToolPanel extends JPanel implements ActionListener {
    private IconButton moveAnimationButton;
    private IconButton zoomAnimationButton;
    private IconButton imageAnimationButton;
    private PaintPanel showPanel;


    public AnimationToolPanel(PaintPanel showPanel) {
        this.showPanel = showPanel;
        initialize();
    }

    private void initialize() {
        setBackground(Color.white);
        setPreferredSize(new Dimension(140, 600));
        Border border = BorderFactory.createLineBorder(Color.black, 10);
        Font font = new Font("SansSerif", Font.BOLD, 20);
        setBorder(BorderFactory.createTitledBorder(border, "animation.Animation tools", 2, 0, font, Color.red));

        moveAnimationButton = new IconButton("moveAnimation");
        zoomAnimationButton = new IconButton("zoomAnimation");
        imageAnimationButton = new IconButton("imageAnimation");

        moveAnimationButton.setPreferredSize(new Dimension(150, 150));
        zoomAnimationButton.setPreferredSize(new Dimension(150, 150));
        imageAnimationButton.setPreferredSize(new Dimension(150, 150));

        add(moveAnimationButton);
        add(zoomAnimationButton);
        add(imageAnimationButton);

        moveAnimationButton.addActionListener(this);
        zoomAnimationButton.addActionListener(this);
        imageAnimationButton.addActionListener(this);


    }

    @Override
    public void actionPerformed(ActionEvent e) {


        if (showPanel.clickedShape != null) {
            if (e.getSource() == moveAnimationButton) {


                MoveAnimation moveAnimation = new MoveAnimation(10, new Point(200, 500), showPanel.clickedShape);
                moveAnimation.setRepeatable(true);
                showPanel.clickedShape.addAnimation(moveAnimation);


            }
            if (e.getSource() == zoomAnimationButton) {

                ZoomAnimation zoomAnimation = new ZoomAnimation(10, 2, showPanel.clickedShape);
                zoomAnimation.setRepeatable(true);
                showPanel.clickedShape.addAnimation(zoomAnimation);


            }
            if (e.getSource() == imageAnimationButton) {
                if (showPanel.clickedShape.getClass() == Image.class) {
                    showPanel.animating = false;

                    ImageAnimation.showPanel = showPanel;
                    ImageAnimation imageAnimation = new ImageAnimation(100);
                    imageAnimation.setRepeatable(true);
                    showPanel.clickedShape.addAnimation(imageAnimation);


                }

            }
            showPanel.repaint();
            showPanel.validate();
        }
    }
}
