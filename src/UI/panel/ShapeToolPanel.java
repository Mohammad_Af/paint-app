package UI.panel;

import UI.IconButton;
import shape.*;
import shape.Image;
import shape.Point;
import shape.Rectangle;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class ShapeToolPanel extends JPanel implements ActionListener {

    private IconButton addCircleButton;
    private IconButton addLineButton;
    private IconButton addRecButton;
    private IconButton addImageButton;
    private PaintPanel showPanel;

    public ShapeToolPanel(PaintPanel showPanel){
        this.showPanel=showPanel;
        initialize();
    }

    private void initialize() {
        setBackground(Color.white);
        Border border=BorderFactory.createLineBorder(Color.black,10);
        Font font = new Font("SansSerif", Font.BOLD, 20);
        setPreferredSize(new Dimension(110,600));
        setBorder(BorderFactory.createTitledBorder(border,"shape.Shape tools",2,0,font,Color.red));

        addCircleButton=new IconButton("circle");
        addRecButton=new IconButton("rectangle");
        addLineButton=new IconButton("line");
        addImageButton=new IconButton("image");


        add(addCircleButton);
        add(addRecButton);
        add(addLineButton);
        add(addImageButton);

        addRecButton.addActionListener(this);
        addCircleButton.addActionListener(this);
        addLineButton.addActionListener(this);
        addImageButton.addActionListener(this);
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==addCircleButton){

            Circle circle=new Circle(new Point(100,100),100);
            showPanel.addShape(circle);


        }
        if(e.getSource()==addImageButton){

            try {
                Image image=new Image(new Point(200,200),100,100, Color.white);
                showPanel.addShape(image);
            } catch (IOException e1) {
                e1.printStackTrace();
            }


        }
        if(e.getSource()==addLineButton){
            Line line=new Line(new Point(20,20),new Point(80,80));
            showPanel.addShape(line);

        }
        if(e.getSource()==addRecButton){

            Rectangle rectangle=new Rectangle(new Point(200,200),100,50);
            showPanel.addShape(rectangle);

        }
        showPanel.repaint();
    }
}
