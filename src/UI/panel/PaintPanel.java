package UI.panel;

import shape.Line;
import shape.Shape;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public class PaintPanel extends JPanel implements MouseMotionListener, MouseListener {

    private ArrayList<shape.Shape> shapes = new ArrayList<>();
    private shape.Point previousMouseLocation = null;
    private shape.Shape selectedShape = null;
    Shape clickedShape = null;
    boolean animating = false;


    protected PaintPanel() {

        initialize();
    }

    private void initialize() {
        Border border = BorderFactory.createLineBorder(Color.black, 10);
        Font font = new Font("SansSerif", Font.BOLD, 20);

        setBorder(BorderFactory.createTitledBorder(border, "Show UI.UI.panel", 2, 0, font, Color.red));

        Thread animationThread = new Thread(() -> {
            while (true) {
                if (!animating) {
                    try {
                        Thread.sleep(1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    continue;
                }

                for (Shape cur : shapes) {
                    try {
                        cur.step();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                try {
                    repaint();
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });


        addMouseMotionListener(this);
        addMouseListener(this);
        animationThread.start();


    }


    @Override
    public void mouseDragged(MouseEvent e) {
        shape.Point curLocation = new shape.Point(e.getX(), e.getY());
        shape.Point dif = curLocation.subtract(previousMouseLocation);
        previousMouseLocation = curLocation;
        if (selectedShape != null) {
            if(selectedShape.getClass()== Line.class){
                ((Line)selectedShape).end = ((Line) selectedShape).end.add(dif);
                ((Line) selectedShape).start=((Line) selectedShape).start.add(dif);
            }
            selectedShape.setLocation(selectedShape.getLocation().add(dif));
        }
        if(clickedShape!=null && clickedShape.solidColor!=null) {                                          //UN Selecte while dragging..........................
            clickedShape.borderColor=null;
            clickedShape=null;
        }

        repaint();
    }


    @Override
    public void mousePressed(MouseEvent e) {

        previousMouseLocation = new shape.Point(e.getX(), e.getY());
        for (Shape cur : shapes) {
            if (cur.isIn(previousMouseLocation)) {
                selectedShape = cur;
            }
        }

    }

    @Override
    protected void paintComponent(Graphics G) {
        super.paintComponent(G);
        for (Shape cur : shapes) {
            cur.render((Graphics2D) G);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {

        previousMouseLocation = new shape.Point(e.getX(), e.getY());
        Iterator<shape.Shape> iterator = shapes.iterator();
        boolean found = false;
        while (iterator.hasNext()) {
            shape.Shape cur = iterator.next();
            if (cur.isIn(previousMouseLocation)) {
                found = true;
                if (clickedShape == null) {
                    clickedShape = cur;
                    clickedShape.borderColor = Color.black;
                    break;
                } else {
                    clickedShape.borderColor = null;
                    if (!cur.equals(clickedShape)) {
                        cur.borderColor = Color.black;
                        clickedShape = cur;
                        break;
                    } else {
                        clickedShape = null;
                    }
                }
            }
        }
        if (!found && clickedShape != null) {
            clickedShape.borderColor = null;
            clickedShape = null;
        }

        repaint();
        validate();


    }


    @Override
    public void mouseReleased(MouseEvent e) {

        previousMouseLocation = null;
        selectedShape = null;
    }


    public boolean getAnimating() {
        return animating;
    }

    public void setAnimating(boolean animating) {
        this.animating = animating;
    }

    public void addShape(shape.Shape shape) {
        shapes.add(shape);
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }

    public Shape getClickedShape() {
        return clickedShape;
    }
}
