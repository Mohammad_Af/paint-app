package UI.panel;

import animation.Animation;
import animation.ImageAnimation;
import animation.MoveAnimation;
import animation.ZoomAnimation;
import shape.Image;
import shape.Point;
import shape.Shape;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

public class AnimationPropertyPanel extends JPanel implements ActionListener {

    private Shape clickedShape;
    private JComboBox<Animation> animations = new JComboBox<>();
    private JButton selectButton;
    private PaintPanel paintPanel;

    public AnimationPropertyPanel(PaintPanel paintPanel) {
        clickedShape = paintPanel.clickedShape;
        this.paintPanel = paintPanel;
        initialize();
        setNull();
    }

    private void initialize() {
        Border border = BorderFactory.createLineBorder(Color.black, 10);
        Font font = new Font("SansSerif", Font.BOLD, 20);
        setBorder(BorderFactory.createTitledBorder(border, "animation.Animation", 2, 0, font, Color.red));
        setPreferredSize(new Dimension(500, 200));
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        animations.addActionListener(this);
        animations.setPreferredSize(new Dimension(180, 30));

    }

    public void setNull() {

        removeAll();
        JPanel panel = new JPanel();
        panel.setLayout(new GridBagLayout());
        panel.add(new JLabel("No shape choosed"));
        add(panel);

        validate();
    }

    public void showProperty() {

        removeAll();
        initialize();                                                      // chera age ino nazari vaghti ro ye shekl 2 bar click mikoni kharab mishe???

        animations.removeAllItems();

        clickedShape = paintPanel.clickedShape;

        JPanel panel = new JPanel();
        panel.setLayout(new GridBagLayout());


        for (int i = 0; i < clickedShape.getAnimations().size(); i++) {
            animations.addItem(clickedShape.getAnimations().get(i));
        }

        panel.add(new JLabel("shape's animations : "));
        panel.add(animations);

        JPanel panel1 = new JPanel();
        panel1.setLayout(new GridBagLayout());
        selectButton = new JButton("Select");
        selectButton.addActionListener(this);
        panel1.add(selectButton);


        add(panel);
        add(panel1);


        validate();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == selectButton) {


            removeAll();


            repaint();

            Animation animation = (Animation) animations.getSelectedItem();


            JPanel jPanel1 = new JPanel();
            jPanel1.setLayout(new GridBagLayout());
            JCheckBox isActive = new JCheckBox("Active");
            JCheckBox repeatable = new JCheckBox("Repeatable");
            jPanel1.add(repeatable);
            jPanel1.add(isActive);


            JPanel jPanel2 = new JPanel();
            jPanel2.setLayout(new GridBagLayout());
            JTextField time = new JTextField();
            time.setPreferredSize(new Dimension(80, 30));
            time.setFont(new Font("SansSerif", Font.BOLD, 20));
            jPanel2.add(new JLabel("time : "));
            jPanel2.add(time);
            jPanel2.add(new JLabel(" ( 1 to 50 prefered )"));


            add(jPanel1);
            add(jPanel2);


            assert animation != null;
            if (animation.getClass() == ZoomAnimation.class) {
                ZoomAnimation zoomAnimation = ((ZoomAnimation) animation);

                JTextField size = new JTextField();
                size.setPreferredSize(new Dimension(80, 30));
                size.setFont(new Font("SansSerif", Font.BOLD, 20));
                JPanel panel1 = new JPanel();
                panel1.setLayout(new GridBagLayout());
                panel1.add(new JLabel("scale : "));
                panel1.add(size);


                JPanel panel2 = new JPanel();
                JButton button = new JButton("apply");
                button.addActionListener(e15 -> {
                    if (!paintPanel.animating) {
                        zoomAnimation.setNesbat(Double.parseDouble(size.getText()));
                        if (isActive.isSelected() != zoomAnimation.getAnimating()) {
                            zoomAnimation.setAnimating(!zoomAnimation.getAnimating());
                        }
                        if (repeatable.isSelected() != zoomAnimation.isRepeatable()) {
                            zoomAnimation.setRepeatable(!zoomAnimation.isRepeatable());
                        }
                        animation.setStepDelay(Integer.parseInt(time.getText()));

                        paintPanel.repaint();
                    } else {
                        JOptionPane.showMessageDialog(new JFrame(), "STOP animating first");
                    }

                });
                panel2.add(button);

                add(panel1);
                add(panel2);
            }
            if (animation.getClass() == MoveAnimation.class) {

                MoveAnimation moveAnimation = ((MoveAnimation) animation);

                JTextField xVector = new JTextField();
                JTextField yVector = new JTextField();
                xVector.setPreferredSize(new Dimension(80, 30));
                yVector.setPreferredSize(new Dimension(80, 30));
                xVector.setFont(new Font("SansSerif", Font.BOLD, 20));
                yVector.setFont(new Font("SansSerif", Font.BOLD, 20));

                JPanel panel1 = new JPanel();
                panel1.setLayout(new GridBagLayout());
                panel1.add(new JLabel("x vector : "));
                panel1.add(xVector);
                panel1.add(new JLabel(" y vector : "));
                panel1.add(yVector);

                JPanel panel2 = new JPanel();
                panel2.setLayout(new GridBagLayout());
                JButton button = new JButton("apply");
                button.addActionListener(e14 -> {
                    if (!paintPanel.animating) {
                        moveAnimation.setVector(new Point(Integer.parseInt(xVector.getText()), Integer.parseInt(yVector.getText())));
                        if (isActive.isSelected() != moveAnimation.isAnimating()) {
                            moveAnimation.setAnimating(!moveAnimation.isAnimating());
                        }
                        if (repeatable.isSelected() != moveAnimation.isRepeatable()) {
                            moveAnimation.setRepeatable(!moveAnimation.isRepeatable());
                        }
                        animation.setStepDelay(Integer.parseInt(time.getText()));

                        paintPanel.repaint();
                    } else {
                        JOptionPane.showMessageDialog(new JFrame(), "STOP animating first");
                    }

                });
                panel2.add(button);
                add(panel1);
                add(panel2);
            }
            if (animation.getClass() == ImageAnimation.class) {
                ImageAnimation imageAnimation = ((ImageAnimation) animation);
                Image image = ((Image) clickedShape);

                JPanel panel1 = new JPanel();
                panel1.setLayout(new GridBagLayout());
                JButton jButton = new JButton("apply changes");
                jButton.addActionListener(e13 -> {
                    if (!paintPanel.animating) {
                        if (isActive.isSelected() != imageAnimation.isAnimating()) {
                            imageAnimation.setAnimating(!imageAnimation.isAnimating());
                        }
                        if (repeatable.isSelected() != imageAnimation.isRepeatable()) {
                            imageAnimation.setRepeatable(!imageAnimation.isRepeatable());
                        }
                        animation.setStepDelay(Integer.parseInt(time.getText()));

                        paintPanel.repaint();
                    } else {
                        JOptionPane.showMessageDialog(new JFrame(), "STOP animating first");
                    }

                });
                panel1.add(jButton);
                add(panel1);


                JComboBox<BufferedImage> images = new JComboBox<>();
                images.setPreferredSize(new Dimension(150, 30));
                for (int i = 0; i < image.getImageList().size(); i++) {
                    images.addItem(image.getImageList().get(i));
                }
                JPanel panel = new JPanel();
                panel.setLayout(new GridBagLayout());
                panel.add(new JLabel("list of pictures : "));
                panel.add(images);


                JPanel panel2 = new JPanel();
                panel2.setLayout(new GridBagLayout());
                JButton jButton1 = new JButton("add picture");
                jButton1.addActionListener(e12 -> {
                    if (!paintPanel.animating) {
                        ImageAnimation.showPanel = paintPanel;
                        ImageAnimation.initializ(image);
                        images.addItem(image.getImageList().get(image.getImageList().size() - 1));
                    } else {
                        JOptionPane.showMessageDialog(new JFrame(), "STOP animating first");
                    }

                });
                panel2.add(jButton1);

                JPanel panel3 = new JPanel();
                panel3.setLayout(new GridBagLayout());
                JButton button = new JButton("remove selected picture");
                button.addActionListener(e1 -> {
                    if (!paintPanel.animating) {
                        image.getImageList().remove(images.getSelectedItem());
                        images.removeItem(images.getSelectedItem());
                    } else {
                        JOptionPane.showMessageDialog(new JFrame(), "STOP animating first");
                    }

                });
                panel3.add(button);


                add(panel2);
                add(panel);
                add(panel3);

                paintPanel.repaint();


            }


        }


        validate();

        paintPanel.repaint();
    }
}
