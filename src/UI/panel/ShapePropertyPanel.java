package UI.panel;

import shape.*;
import shape.Image;
import shape.Point;
import shape.Rectangle;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ShapePropertyPanel extends JPanel implements ActionListener {

    private JComboBox<String> comboBox;
    private Color[] colors;
    private JTextField xTextField;
    private JTextField yTextField;
    private List<JPanel> panels = new ArrayList<>();
    private JButton applyButton;
    private shape.Shape clickedShape;
    private JTextField radius;
    private JTextField recWidth;
    private JTextField recHeight;
    private JButton changeImage;
    private JTextField imageWidth;
    private JTextField imageHeight;
    private JTextField xVector;
    private JTextField yVector;
    private PaintPanel paintPanel;


    public ShapePropertyPanel(PaintPanel paintPanel) {

        clickedShape = paintPanel.getClickedShape();
        this.paintPanel = paintPanel;
        initialize();
        setNull();
    }

    private void initialize() {

        Border border = BorderFactory.createLineBorder(Color.black, 10);
        Font font = new Font("SansSerif", Font.BOLD, 20);
        setBorder(BorderFactory.createTitledBorder(border, "shape.Shape", 2, 0, font, Color.red));

        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        setPreferredSize(new Dimension(300, 200));

        String[] stringColors = {"Red", "Yellow", "Blue", "Green", "Cyan", "Pink"};
        colors = new Color[]{Color.red, Color.yellow, Color.blue, Color.GREEN, Color.cyan, Color.pink};

        comboBox = new JComboBox<>(stringColors);
        comboBox.setPreferredSize(new Dimension(160, 40));

        xTextField = new JTextField();
        xTextField.setPreferredSize(new Dimension(60, 30));
        xTextField.setHorizontalAlignment(JTextField.CENTER);
        xTextField.setFont(font);

        yTextField = new JTextField();
        yTextField.setPreferredSize(new Dimension(60, 30));
        yTextField.setHorizontalAlignment(JTextField.CENTER);
        yTextField.setFont(font);

        JPanel panel1 = new JPanel();
        panel1.setLayout(new GridBagLayout());
        panel1.add(new JLabel("Color : "));
        panel1.add(comboBox);
        panels.add(panel1);

        JPanel panel2 = new JPanel();
        panel2.setLayout(new GridBagLayout());
        panel2.add(new JLabel("Location x : "));
        panel2.add(xTextField);
        panel2.add(new JLabel(" Location y : "));
        panel2.add(yTextField);
        panels.add(panel2);

    }

    public void setNull() {

        removeAll();
        JPanel panel = new JPanel();
        panel.setLayout(new GridBagLayout());
        panel.add(new JLabel("No shape choosed"));
        add(panel);
        validate();

    }

    public void showProperty() {

        removeAll();
        updateUI();

        clickedShape = paintPanel.clickedShape;


        xTextField.setText(String.valueOf((int) clickedShape.location.x));
        yTextField.setText(String.valueOf((int) clickedShape.location.y));
        comboBox.setSelectedIndex(clickedShape.getColorIndex());


        add(panels.get(0));
        add(panels.get(1));


        if (clickedShape.getClass() == Circle.class) {
            JPanel panel = new JPanel();
            panel.setLayout(new GridBagLayout());
            radius = new JTextField();
            radius.setText(String.valueOf((int) (((Circle) clickedShape).getRadius())));
            radius.setPreferredSize(new Dimension(80, 40));
            radius.setHorizontalAlignment(JTextField.CENTER);
            radius.setFont(new Font("SansSerif", Font.BOLD, 20));
            panel.add(new JLabel("radius : "));
            panel.add(radius);
            radius.addActionListener(this);
            add(panel);

        }

        if (clickedShape.getClass() == Rectangle.class) {
            JPanel panel = new JPanel();
            panel.setLayout(new GridBagLayout());
            recWidth = new JTextField();
            recWidth.setText(String.valueOf((int) ((Rectangle) clickedShape).width));
            recWidth.setPreferredSize(new Dimension(70, 30));
            recHeight = new JTextField();
            recHeight.setText(String.valueOf((int) ((Rectangle) clickedShape).height));
            recHeight.setPreferredSize(new Dimension(70, 30));
            recWidth.addActionListener(this);
            recHeight.addActionListener(this);
            panel.add(new JLabel("rec width : "));
            panel.add(recWidth);
            panel.add(new JLabel("rec height : "));
            panel.add(recHeight);
            add(panel);


        }

        if (clickedShape.getClass() == Line.class) {

            JPanel panel = new JPanel();
            panel.setLayout(new GridBagLayout());
            xVector = new JTextField();
            yVector = new JTextField();
            xVector.setPreferredSize(new Dimension(70, 30));
            yVector.setPreferredSize(new Dimension(70, 30));
            xVector.setText(String.valueOf((int) ((Line) clickedShape).end.x - ((Line) clickedShape).start.x));
            yVector.setText(String.valueOf((int) ((Line) clickedShape).end.y - ((Line) clickedShape).start.y));

            panel.add(new JLabel("X vector : "));
            panel.add(xVector);
            panel.add(new JLabel("Y vecor : "));
            panel.add(yVector);
            xVector.addActionListener(this);
            yVector.addActionListener(this);

            add(panel);

        }

        if (clickedShape.getClass() == Image.class) {

            JPanel panel = new JPanel();
            panel.setLayout(new GridBagLayout());
            changeImage = new JButton("change image");
            changeImage.addActionListener(this);
            panel.add(changeImage);


            JPanel panel1 = new JPanel();
            panel1.setLayout(new GridBagLayout());
            imageWidth = new JTextField();
            imageWidth.setPreferredSize(new Dimension(70, 30));
            imageHeight = new JTextField();
            imageHeight.setPreferredSize(new Dimension(70, 30));

            imageWidth.setText(String.valueOf((int) ((Image) clickedShape).width));
            imageHeight.setText(String.valueOf((int) ((Image) clickedShape).height));

            imageWidth.addActionListener(this);
            imageHeight.addActionListener(this);
            panel1.add(new JLabel("image width : "));
            panel1.add(imageWidth);
            panel1.add(new JLabel("image height : "));
            panel1.add(imageHeight);


            add(panel);
            add(panel1);


        }


        JPanel panel = new JPanel();
        panel.setLayout(new GridBagLayout());
        applyButton = new JButton("Apply");
        applyButton.addActionListener(this);
        panel.add(applyButton);
        add(panel);


        validate();

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == applyButton) {
            if (!paintPanel.animating) {
                clickedShape.location = new Point(Integer.parseInt(xTextField.getText()), Integer.parseInt(yTextField.getText()));
                clickedShape.solidColor = colors[comboBox.getSelectedIndex()];
                clickedShape.setColorIndex(comboBox.getSelectedIndex());

                if (clickedShape.getClass() == Circle.class) {
                    ((Circle) clickedShape).setRadius(Integer.parseInt(radius.getText()));

                }
                if (clickedShape.getClass() == Rectangle.class) {
                    ((Rectangle) clickedShape).setWidth(Integer.parseInt(recWidth.getText()));
                    ((Rectangle) clickedShape).setHeight(Integer.parseInt(recHeight.getText()));

                }
                if (clickedShape.getClass() == Line.class) {
                    Line line = ((Line) clickedShape);
                    line.start = clickedShape.location;
                    line.end = ((Line) clickedShape).start.add(new Point(Integer.parseInt(xVector.getText()), Integer.parseInt(yVector.getText())));
                    System.out.println(line.start.x + " " + line.start.y);
                    System.out.println(line.end.x + " " + line.end.y);

                }
                if (clickedShape.getClass() == Image.class) {

                    ((Image) clickedShape).setWidth(Integer.parseInt(imageWidth.getText()));
                    ((Image) clickedShape).setHeight(Integer.parseInt(imageHeight.getText()));

                }
            } else {
                JOptionPane.showMessageDialog(new JFrame(), "STOP animating first");
            }

            paintPanel.repaint();

        }
        if (e.getSource() == changeImage) {
            if (!paintPanel.animating) {
                JFileChooser chooser = new JFileChooser();
                FileNameExtensionFilter filter = new FileNameExtensionFilter("image", "jpg", "png", "gif");
                chooser.setFileFilter(filter);
                if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                    try {
                        Image image = ((Image) clickedShape);
                        image.getImageList().remove(image.getImage());
                        image.getImageList().add(ImageIO.read(new File(chooser.getSelectedFile().getAbsolutePath())));
                        image.setImage(ImageIO.read(new File(chooser.getSelectedFile().getAbsolutePath())));

                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            } else {
                JOptionPane.showMessageDialog(new JFrame(), "STOP animating first");
            }
        }

    }
}
