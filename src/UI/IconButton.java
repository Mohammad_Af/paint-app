package UI;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class IconButton extends JButton{

    private String shapeName;

    public IconButton(String shapeName){
        this.shapeName=shapeName;
        initialize();
    }

    private void initialize() {
        setPreferredSize(new Dimension(100,100));
        setBackground(Color.WHITE);
        setBorder(null);


    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        if(shapeName.equals("circle")){
            g.setColor(Color.red);
            g.fillOval(0,0,getWidth(),getHeight());
        }
        if(shapeName.equals("rectangle")){

            g.setColor(Color.blue);
            g.fillRect(10,10,getWidth()-20,getHeight()-20);
        }
        if(shapeName.equals("line")){
            g.setColor(Color.green);
            ((Graphics2D)g).setStroke(new BasicStroke(5));
            g.drawLine(0,0,getWidth(),getHeight());

        }
        if(shapeName.equals("image")){
            try {
                BufferedImage image=ImageIO.read(new File("images"+File.separator+"Tamrin 1.png"));
                g.drawImage(image,10,10,getWidth()-20,getHeight()-20,this);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if(shapeName.equals("moveAnimation")){
            try {
                BufferedImage image=ImageIO.read(new File("images"+File.separator+"Tamrin 1.2.png"));
                g.drawImage(image,10,10,getWidth()-20,getHeight()-20,this);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if(shapeName.equals("zoomAnimation")){

            try {
                BufferedImage image=ImageIO.read(new File("images"+File.separator+"Tamrin 1.4.png"));
                g.drawImage(image,10,10,getWidth()-20,getHeight()-20,this);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        if(shapeName.equals("imageAnimation")){

            try {
                BufferedImage image=ImageIO.read(new File("images"+File.separator+"Tamrin 1.3.png"));
                g.drawImage(image,10,10,getWidth()-20,getHeight()-20,this);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
