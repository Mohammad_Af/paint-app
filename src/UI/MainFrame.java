package UI;

import UI.panel.*;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainFrame extends JFrame implements ActionListener {

    private PaintPanel showPanel;
    private ShapePropertyPanel shapePropertyPanel;
    private AnimationPropertyPanel animationPropertyPanel;
    private JButton runButton;


    public MainFrame() {
        initialize();
    }

    private void initialize() {
        setSize(1100, 900);
        setLocation(300, 100);
        // setResizable(false);
        JPanel content = (JPanel) getContentPane();
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("Paint Application");

        JPanel panel = new JPanel();
        JPanel panel1 = new JPanel();
        JPanel emPanelP = new JPanel();
        JPanel panel2 = new JPanel();

        content.setLayout(new BoxLayout(content, BoxLayout.PAGE_AXIS));

        panel.setPreferredSize(new Dimension(1100, 10));
        panel1.setPreferredSize(new Dimension(1100, 600));
        panel2.setPreferredSize(new Dimension(1100, 300));

        panel1.setLayout(new BoxLayout(panel1, BoxLayout.LINE_AXIS));
        panel2.setLayout(new BoxLayout(panel2, BoxLayout.LINE_AXIS));


        showPanel = new PaintPanel() {

            @Override
            public void validate() {

                updateShapePropertyPanel();
                updateAnimationPropertyPanel();
                super.validate();
            }
        };


        AnimationToolPanel animationToolPanel = new AnimationToolPanel(showPanel);
        JPanel emPanel = new JPanel();
        JPanel emPanel2 = new JPanel();
        ShapeToolPanel shapeToolPanel = new ShapeToolPanel(showPanel);


        showPanel.setPreferredSize(new Dimension(600, 600));
        emPanel.setPreferredSize(new Dimension(5, 600));
        emPanel2.setPreferredSize(new Dimension(5, 600));


        panel1.add(animationToolPanel);
        panel1.add(emPanel);
        panel1.add(showPanel);
        panel1.add(emPanel2);
        panel1.add(shapeToolPanel);

        //Panel 2....................
        shapePropertyPanel = new ShapePropertyPanel(showPanel);
        JPanel emJPanel = new JPanel();
        JPanel emJPanel2 = new JPanel();
        animationPropertyPanel = new AnimationPropertyPanel(showPanel);
        JPanel runPanel = new JPanel();


        Border border = BorderFactory.createLineBorder(Color.black, 10);
        Font font = new Font("SansSerif", Font.BOLD, 20);

        runPanel.setBorder(BorderFactory.createTitledBorder(border, "Run", 2, 0, font, Color.red));


        emJPanel.setPreferredSize(new Dimension(10, 200));
        emJPanel2.setPreferredSize(new Dimension(10, 200));
        runPanel.setPreferredSize(new Dimension(100, 200));

        runButton = new JButton("Run");
        runButton.setPreferredSize(new Dimension(70, 30));
        runPanel.setLayout(new GridBagLayout());
        runPanel.add(runButton);

        panel2.add(shapePropertyPanel);
        panel2.add(emJPanel);
        panel2.add(animationPropertyPanel);
        panel2.add(emJPanel2);
        panel2.add(runPanel);

        runButton.addActionListener(this);


        content.add(panel);
        content.add(panel1);
        content.add(emPanelP);
        content.add(panel2);


        setVisible(true);
    }

    private void updateAnimationPropertyPanel() {
        if (showPanel != null) {
            if (showPanel.getClickedShape() != null) {
                    animationPropertyPanel.showProperty();

            } else {
                animationPropertyPanel.setNull();
            }

        }
    }

    private void updateShapePropertyPanel() {
        if (showPanel != null) {
            if (showPanel.getClickedShape() != null) {
                shapePropertyPanel.showProperty();
            } else {
                shapePropertyPanel.setNull();
            }
        }

    }


    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == runButton) {
            showPanel.setAnimating(!showPanel.getAnimating());

        }
        showPanel.repaint();
    }


}
